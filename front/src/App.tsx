import React, { useState, useRef } from 'react';
import { ApolloProvider, useQuery, useSubscription } from '@apollo/react-hooks';
import Select, { ValueType, OptionsType } from 'react-select';
import ReactMarkdown from 'react-markdown';
import Editor from '@monaco-editor/react';
import gql from 'graphql-tag';
import { WebSocketLink } from 'apollo-link-ws';
import { split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { getMainDefinition } from 'apollo-utilities';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';

const wsLink = new WebSocketLink({
  uri: `ws://localhost:3000/graphql`,
  options: {
    reconnect: true,
  },
});

const httpLink = new HttpLink({
  uri: 'http://localhost:3000/graphql',
});

const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);

const client = new ApolloClient({ link, cache: new InMemoryCache() });

const QUERY_TASK_SELECT = gql`
  {
    tasks {
      id
      name
    }
  }
`;

const QUERY_TASK_FULL = gql`
  query Task($id: ID) {
    task(id: $id) {
      description
      complexity
    }
  }
`;

const SUBSCRIPTION_VALIDATE_SUBMISSION = gql`
  subscription validate($task: ID, $code: String) {
    validate(task: $task, code: $code) {
      id
      points
      tests {
        id
        category
        verdict
      }
      completed
    }
  }
`;

export type TestVerdict = 'OK' | 'WA' | 'RE' | 'TL' | 'ML';

export type TestResult = {
  id: number;
  category: string;
  verdict: TestVerdict;
};

export type ValidationResult = {
  id: string;
  points: number;
  tests: TestResult[];
  completed: boolean;
};

type Option = ValueType<{
  label: string;
  value: string;
}>;

type Options = OptionsType<{ label: string; value: string }>;

const TaskSelect = ({
  selected,
  setSelected,
}: {
  selected: Option;
  setSelected: (selected: Option) => void;
}) => {
  const { loading, error, data } = useQuery(QUERY_TASK_SELECT);

  if (loading) return <p>Loading</p>;
  if (error) return <p>Error</p>;

  let options = data.tasks.map(
    ({ name, id }: { name: string; id: string }) => ({
      value: id,
      label: name,
    }),
  );

  return <Select value={selected} options={options} onChange={setSelected} />;
};

const TaskFull = ({ selected }: { selected: Option | undefined }) => {
  let skip = selected === undefined || selected == null;

  const { loading, error, data } = useQuery(QUERY_TASK_FULL, {
    skip: skip,
    // @ts-ignore
    variables: { id: selected && selected.value },
  });

  if (loading) return <h1>Loading</h1>;
  if (error) return <h1>Error</h1>;

  if (skip) {
    return <h1>Select task first</h1>;
  }

  // @ts-ignore
  let label = selected.label;
  let task = data.task;

  return (
    <>
      <h1>{label}</h1>
      <h2>Complexity: {task.complexity}</h2>
      <ReactMarkdown source={task.description} />
    </>
  );
};

const SubmissionResult = ({
  submission,
  selected,
}: {
  submission: string | undefined;
  selected: Option | undefined;
}) => {
  const [result, setResult] = useState<ValidationResult>();

  let skip =
    submission === undefined || selected === undefined || selected == null;

  // @ts-ignore
  let id = selected && selected.value;

  console.log(id);

  const { data, loading, error } = useSubscription(
    SUBSCRIPTION_VALIDATE_SUBMISSION,
    {
      variables: { task: id, code: submission },
      onSubscriptionData: ({ subscriptionData: { data } }) =>
        setResult(data.validate),
      skip,
    },
  );

  if (loading) return <div>Loading</div>;
  console.log(error);
  if (error) return <div>Error</div>;

  if (skip) {
    return <div>Error</div>;
  }

  // setResult(data.validate);

  return (
    <div>
      {result && (
        <>
          <p>Points: {result.points}</p>
          <p>Completed: {result.completed ? "Yes" : "No"}</p>
          <ul>
            {result.tests.map((result) => {
              return (
                <li>
                  <p>{result.category}</p>
                  <p>
                    {result.id} {result.verdict}
                  </p>
                </li>
              );
            })}
          </ul>
        </>
      )}
    </div>
  );
};

const App = () => {
  const [isEditorReady, setIsEditorReady] = useState(false);
  const [submission, setLastSubmission] = useState<string | undefined>(
    undefined,
  );
  const valueGetter = useRef<() => string>();
  const [selected, setSelected] = useState<Option | undefined>();

  const handleEditorDidMount = (_valueGetter: () => string) => {
    setIsEditorReady(true);
    valueGetter.current = _valueGetter;
  };

  const submit = () => {
    if (valueGetter.current === undefined) {
      return;
    }

    setLastSubmission(valueGetter.current());
  };

  return (
    <ApolloProvider client={client}>
      <div>
        <header>
          <TaskSelect selected={selected} setSelected={setSelected} />
        </header>
        <main>
          <TaskFull selected={selected} />
          {selected !== undefined && selected !== null && (
            <Editor
              theme="dark"
              height="60vh"
              language="python"
              editorDidMount={handleEditorDidMount}
            />
          )}
          {isEditorReady && selected !== undefined && selected !== null && (
            <button onClick={submit}>Submit</button>
          )}
          {submission !== undefined &&
            selected !== undefined &&
            selected !== null && (
              <SubmissionResult selected={selected} submission={submission} />
            )}
        </main>
      </div>
    </ApolloProvider>
  );
};

export default App;
