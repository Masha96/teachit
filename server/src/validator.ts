import { v4 as uuidv4 } from 'uuid';
import { Test, TestVerdict, TestResult, Task, ValidationResult } from './types';
import {
  promises as fs,
  createReadStream,
  createWriteStream,
  WriteStream,
  ReadStream,
} from 'fs';
import { join, resolve } from 'path';
import { spawn, exec as execCB } from 'child_process';
import { Stream } from 'stream';
import { promisify } from 'util';
import { PubSub } from 'apollo-server';

const exec = promisify(execCB);

const runTest = async (
  update: (test: TestResult) => void,
  task: Task,
  test: Test,
  codePath: string,
  solutionPath: string,
) => {
  let inStream = await fs.open(test.questionPath, 'r');

  let outputPath = join(solutionPath, test.id.toString());
  let errPath = join(solutionPath, test.id.toString() + '.err');

  let outStream = await fs.open(outputPath, 'w');
  let errStream = await fs.open(errPath, 'w');

  let run = new Promise((resolve, reject) => {
    let child = spawn('python', [codePath], {
      stdio: [inStream.fd, outStream.fd, errStream.fd],
    });

    child.on('exit', async () => {
      await inStream.close();
      await outStream.close();
      await errStream.close();

      resolve();
    });
  });

  await run;

  let checkerPath = join(task.path, 'check.exe');
  let args = [test.questionPath, outputPath, test.answerPath]
    .map((path) => resolve(path))
    .join(' ');

  let res = '';

  try {
    const { stdout } = await exec(checkerPath + ' ' + args);
    res = stdout;
  } catch (e) {
    const stderr = e.stderr;
    res = stderr;
  }

  let verdict: TestVerdict = 'OK';

  if (res.startsWith('wrong answer')) {
    verdict = 'WA';
  }

  if ((await fs.stat(errPath))['size'] > 0) {
    verdict = 'RE';
  }

  let result: TestResult = {
    id: test.id,
    category: test.category,
    verdict,
  };

  update(result);
};

const runTests = async (
  update: (test: TestResult) => void,
  task: Task,
  codePath: string,
  solutionPath: string,
) => {
  for (let test of task.tests) {
    await runTest(update, task, test, codePath, solutionPath);
  }
};

const _validate = async (
  update: (test: TestResult) => void,
  markCompleted: () => void,
  solutionPath: string,
  code: string,
  task: Task,
) => {
  await fs.mkdir(solutionPath, { recursive: true });
  let codePath = join(solutionPath, 'code.py');

  await fs.writeFile(codePath, code, 'utf-8');
  await runTests(update, task, codePath, solutionPath);

  await fs.rmdir(solutionPath, { recursive: true });

  markCompleted();
};

export const validate = (
  pubsub: PubSub,
  code: string,
  task: Task,
): AsyncIterator<any, any, any> => {
  let uuid = uuidv4();
  let solutionPath = join('./tmp', uuid);

  let result: ValidationResult = {
    id: uuid,
    points: 0,
    tests: [],
    completed: false,
  };

  let updateResult = (test: TestResult) => {
    result.tests.push(test);
    pubsub.publish(uuid, {
      validate: result,
    });
  };

  let markCompleted = () => {
    result.completed = true;
    pubsub.publish(uuid, {
      validate: result,
    });
  }

  _validate(updateResult, markCompleted, solutionPath, code, task).then(() => {});

  setTimeout(
    () =>
      pubsub.publish(uuid, {
        validate: result,
      }),
    20,
  );

  return pubsub.asyncIterator([uuid]);
};
