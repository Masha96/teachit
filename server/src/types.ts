export type Hint = {
  level: number;
  text: string;
  unlockAfter: number;
};

export type Task = {
  id: string;
  name: string;
  description: string;
  complexity: number;
  path: string;
  hints: Hint[];
  tests: Test[];
};

export type TestVerdict = 'OK' | 'WA' | 'RE' | 'TL' | 'ML';

export type TestResult = {
  id: number;
  category: string;
  verdict: TestVerdict;
};

export type ValidationResult = {
  id: string;
  points: number;
  tests: TestResult[];
  completed: boolean;
};

export type Test = {
  id: number;
  category: string;
  questionPath: string;
  answerPath: string;
};
