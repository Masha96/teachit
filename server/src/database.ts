import { promises as fs } from 'fs';
import { join } from 'path';
import { Task, Test } from './types';

const parseTask = async (taskName: string, taskPath: string): Promise<Task> => {
  let taskInfo = JSON.parse(
    await fs.readFile(join(taskPath, 'info.json'), 'utf-8'),
  );

  return {
    id: taskName,
    name: taskInfo['name'],
    description: await fs.readFile(join(taskPath, 'description.md'), 'utf-8'),
    complexity: taskInfo['complexity'],
    path: taskPath,
    hints: taskInfo['hints'].map((hint: any) => {
      return {
        level: hint['level'],
        text: hint['text'],
        unlockAfter: hint['unlockAfter'],
      };
    }),
    tests: await parseTests(taskPath),
  };
};

const parseTestsCategory = async (
  path: string,
  category: string,
): Promise<Test[]> => {
  return (await fs.readdir(path))
    .filter((name) => !name.endsWith('.a'))
    .map((name) => {
      let questionPath = join(path, name);
      let answerPath = join(path, `${name}.a`);
      let id = Number.parseInt(name);

      return {
        id,
        category,
        questionPath,
        answerPath,
      };
    });
};

const parseTests = async (taskPath: string): Promise<Test[]> => {
  let categories = await fs.readdir(join(taskPath, 'tests'));

  let tests = await Promise.all(
    categories.map((category) => {
      let path = join(taskPath, 'tests', category);

      return parseTestsCategory(path, category);
    }),
  );

  return tests.flat();
};

export const parseDatabase = async (databasePath: string): Promise<Task[]> => {
  let taskDirectories = await fs.readdir(databasePath);

  return Promise.all(
    taskDirectories.map((path) => parseTask(path, join(databasePath, path))),
  );
};
