import { gql } from 'apollo-server';

export const typeDefs = gql`
  type Hint {
    level: Int!
    text: String!
    unlockAfter: Int!
  }

  type Task {
    id: ID!
    name: String!
    description: String!
    complexity: Int!
  }

  enum TestVerdict {
    OK
    WA
    RE
    TL
    ML
  }

  type TestResult {
    id: Int!
    category: String!
    verdict: TestVerdict!
  }

  type ValidationResult {
    id: ID!
    points: Int!
    tests: [TestResult!]!
    completed: Boolean!
  }

  type Query {
    tasks: [Task!]!
    task(id: ID): Task
  }

  type Subscription {
    validate(task: ID, code: String): ValidationResult!
  }
`;

export default typeDefs;
