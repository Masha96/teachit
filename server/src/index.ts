import { PubSub } from 'apollo-server';
import schema from './schema';
import express from 'express';
import { ApolloServer, ApolloError } from 'apollo-server-express';
import { parseDatabase } from './database';
import { validate } from './validator';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { createServer } from 'http';

const pubsub = new PubSub();

parseDatabase('./database').then((database) => {
  const resolvers = {
    Query: {
      tasks() {
        return database;
      },
      task(_: void, args: any) {
        let filtered = database.filter((task) => task.id == args.id);
        return filtered.length > 0 ? filtered[0] : null;
      },
    },
    Subscription: {
      validate: {
        subscribe(_: void, args: any): AsyncIterator<any, any, any> {
          let code = args.code;
          let filtered = database.filter((task) => task.id == args.task);

          if (filtered.length > 0) {
            return validate(pubsub, code, filtered[0]);
          } else {
            throw new ApolloError('Task not exists', '404');
          }
        },
      },
    },
  };

  const app = express();
  const server = new ApolloServer({
    typeDefs: schema,
    playground: true,
    resolvers,
  });

  const httpServer = createServer(app);
  server.installSubscriptionHandlers(httpServer);

  server.applyMiddleware({ app, path: '/graphql' });

  httpServer.listen({ port: 3000 }, () => {
    console.log('Server is now running on http://localhost:3000');
  });
});
